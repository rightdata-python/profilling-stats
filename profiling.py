from flask import request
from flask import Flask
# from pyspark.shell import spark
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType
from pyspark.sql.types import ArrayType, DoubleType, BooleanType
from pyspark.sql.functions import col, array_contains
import json
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
spark = SparkSession.builder.master("local").appName("DX_STATS").enableHiveSupport().getOrCreate()
spark.sql("SHOW TABLES").show()
api_v2_cors_config = {
    "origins": ["http://localhost:5000"],
    "methods": ["OPTIONS", "GET", "POST"],
    "allow_headers": ["Authorization", "Content-Type"]
}


@app.route('/analyze', methods=["POST"])
@cross_origin(**api_v2_cors_config)
def analyze():
    content = request.get_json()
    dir = content["preview"]
    if dir is True:
        tablename = "p_" + str(content['guid']) + "_" + str(content['stepId']) + "_" + str(
            content['inputOutputId']) + "_v"
        print(tablename)
    else:
        tablename = "p_" + str(content['guid']) + "_" + str(content['sessionId']) + "_" + str(
            content['stepId']) + "_" + str(content['inputOutputId']) + "_v"
        print(tablename)
    sql = "select * from " + tablename + " limit " + str(content["previewCount"])
    df = spark.sql(sql)
    df = df.drop("DX_MERGE_ROW_COUNT", "DX_PACKET_ID", "DX_ROW_ID");
    columns = df.columns
    list_of_dictionary = [{"field": i} for i in columns]
    result = {"ColumnNames": list_of_dictionary}

    data = df.toPandas()
    stats = data.describe()
    describes = stats.to_dict()
    desc = json.loads(stats.to_json(orient="index"))
    statss = json.loads(stats.to_json(orient='records'))
    df2 = data.isnull().sum()
    notnull = data.notnull().sum()
    zero = (data == 0).sum(axis=0)
    nonzero = (data != 0).sum(axis=0)
    null_result = json.loads(df2.to_json(orient="index"))
    null_result['statsresult'] = 'nullresult'
    not_null = json.loads(notnull.to_json(orient="index"))
    not_null['statsresult'] = 'notnull'
    stats_result = json.loads(stats.to_json(orient='index'))
    stats_result['statsresult'] = 'count'
    coun = stats_result['count']
    coun['statsresult'] = 'count'
    uni = stats_result['std']
    uni['statsresult'] = 'std'
    to = stats_result['mean']
    to['statsresult'] = 'mean'
    freqqq = stats_result['min']
    freqqq['statsresult'] = 'min'
    perc_1 = stats_result['25%']
    perc_1['statsresult'] = '25%'
    perc_2 = stats_result['50%']
    perc_2['statsresult'] = '50%'
    perc_3 = stats_result['75%']
    perc_3['statsresult'] = '75%'
    maxi = stats_result['max']
    maxi['statsresult'] = 'max'
    duk = uni, coun, to, freqqq, perc_1, perc_2, perc_3, maxi
    dukk = list(duk)
    zero_result = json.loads(zero.to_json(orient="index"))
    zero_result['statsresult'] = 'zeroresult'
    non_zero = json.loads(nonzero.to_json(orient='index'))
    non_zero['statsresult'] = 'nonzero'
    cv = null_result, not_null, zero_result, non_zero
    cvv = list(cv)
    fav = dukk + cvv

    data = {'ColumnNames': list_of_dictionary, 'Data': fav}
    return data


if __name__ == '__main__':
    app.run(host='0.0.0.0')  # host = "0.0.0.0",port = specific port0
